'use strict';

const CSVParser = (text, alpha) => {
  const mutatedText = text.substring(text.indexOf("Frame")).split("\n");
  const parsed = mutatedText.slice(1).map((line) => {
    const values = line.split("\t").filter((value) => value !== "");
    return {
      frame: Number(values[0]),
      pos_x: Number(values[1]),
      pos_y: Number(values[2]),
      alpha: alpha ? 1 : Number(values[3]),
    };
  });
  return JSON.stringify(parsed, undefined, 2);
};

if (process.argv.length < 3) {
  console.log("Usage: node " + process.argv[1] + " FILENAME ALPHA");
  process.exit(1);
}

const filename = process.argv[2];
const alpha = process.argv[3] | false;
const exportname = `${filename.split('.').slice(0, -1).join('.')}.json`;

const fs = require("fs");

fs.readFile(filename, "utf8", function (err, data) {
  if (err) throw err;
  console.log("OK: " + filename);
  const parsed = CSVParser(data, alpha)
  fs.writeFileSync(exportname, parsed);
  console.log(`Created ${exportname}`);
});
