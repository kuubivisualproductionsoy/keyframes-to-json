import { useState } from "react";
import styled from "styled-components";

const TextArea = styled.textarea`
  width: 98%;
  height: 500px;
  overflow: auto;
  resize: vertical;
  margin: 2.8em 0px;
  box-sizing: border-box;
  border: none;
  overflow: auto;
  height: auto;
  padding: 8px;
  box-shadow: 0px 4px 10px -8px black;
  font-size: 14px;
  line-height: 16px;

  &::placeholder {
    color: gainsboro;
  }

  &:focus {
    outline: none;
  }
`;

const ResizableTextarea = ({ value, setValue }) => {
  const [rows, setRows] = useState(10);
  const minRows = 10;
  const handleChange = (event) => {
    setValue(event.target.value);

    const textareaLineHeight = 16;
		
		const previousRows = event.target.rows;
  	event.target.rows = minRows; // reset number of rows in textarea 
		
		const currentRows = ~~(event.target.scrollHeight / textareaLineHeight);
    
    if (currentRows === previousRows) {
    	event.target.rows = currentRows;
    }

    setRows(currentRows)
  };
  const onKeyDown = (event) => {
    if (event.keyCode === 9) {
      event.preventDefault();
      const newValue = event.target.value + "\t";
      setValue(newValue);
    }
  };
  return (
    <TextArea
      wrap="off"
      value={value}
      onChange={handleChange}
      onKeyDown={onKeyDown}
      rows={rows}
      placeholder="Put your CSV here"
    />
  );
};

export default ResizableTextarea;
