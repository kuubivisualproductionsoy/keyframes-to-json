const breakpoint = {
  xs: "576px",
  sm: "768px",
  md: "992px",
  lg: "1200px",
};

export const size = {
  xs: "540px",
  sm: "720px",
  md: "960px",
  lg: "1140px",
};

export const device = {
  xs: `(min-width: ${breakpoint.xs})`,
  sm: `(min-width: ${breakpoint.sm})`,
  md: `(min-width: ${breakpoint.md})`,
  lg: `(min-width: ${breakpoint.lg})`,
};
