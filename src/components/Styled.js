import styled from "styled-components";
import { size, device } from "./QuerySizes";

export const Header = styled.h1`
  text-align: center;
`;

export const Row = styled.div`
  display: flex;
`;

export const Col = styled.div`
  flex: 0 0 50%;
`;

export const Container = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  @media ${device.xs} {
    max-width: ${size.xs};
  }
  @media ${device.sm} {
    max-width: ${size.sm};
  }
  @media ${device.md} {
    max-width: ${size.md};
  }
  @media ${device.lg} {
    max-width: ${size.lg};
  }
`;

export const Button = styled.button`
  border: 1px solid #000;
  padding: 5px 15px;
  background: #fff;
  margin: auto;
  display: block;
`;

export const Center = styled.div`
  text-align: center;
  margin-top: 10px;
`;
