export const CSVParser = (text, alpha) => {
  const mutatedText = text.substring(text.indexOf("Frame")).split("\n");
  const parsed = mutatedText.slice(1).map(line => {
    const values = line.split('\t').filter(value => value !== "");
    return {
      frame: Number(values[0]),
      pos_x: Number(values[1]),
      pos_y: Number(values[2]),
      alpha: alpha ? 1 : Number(values[3])
    }
  });
  return JSON.stringify(parsed, undefined, 2);
};
