import React, { useState } from "react";
import { CSVParser } from "./converter/JsonConverter";
import {
  Row,
  Col,
  Header,
  Container,
  Button,
  Center,
} from "./components/Styled";
import TextArea from "./components/TextArea";
import "./assets/styles.css";

const App = () => {
  const [csv, setCSV] = useState("");
  const [json, setJSON] = useState("Your JSON will appear here");
  const [alpha, setAlpha] = useState(false);
  const [copied, setCopied] = useState(false);
  const handleClick = () => {
    const parsed = CSVParser(csv, alpha);
    setJSON(parsed);
  };
  const handleChange = (value) => {
    setCSV(value);
  };
  const handleCheckbox = (event) => {
    setAlpha(event.target.checked);
  };
  const copyToClipboard = () => {
    const el = document.createElement("textarea");
    el.value = json;
    el.setAttribute("readonly", "");
    el.style.position = "absolute";
    el.style.left = "-9999px";
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    setTimeout(() => setCopied(false), 3000);
  };
  return (
    <Container>
      <Header>Convert Keyframes to JSON</Header>
      <Button onClick={handleClick}>Convert</Button>
      <Center>
        <input
          name="alpha"
          type="checkbox"
          checked={alpha}
          onChange={handleCheckbox}
        />
        <label htmlFor="alpha">Set alpha to 1</label>
      </Center>
      <Row>
        <Col>
          <TextArea value={csv} setValue={handleChange} />
        </Col>
        <Col>
          <Button onClick={copyToClipboard}>Copy to clipboard</Button>
          {copied && `Copied!`}
          <pre>
            <code>{json}</code>
          </pre>
        </Col>
      </Row>
    </Container>
  );
};

export default App;
