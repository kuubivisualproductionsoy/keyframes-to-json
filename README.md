# to run browser version

`npm install`

`npm start`

# to just get the json without hassle

`node convert.js [relative path to your csv file] [alpha]`

for example

`node convert.js myfile.txt 1`

should return you with

`OK: myfile.txt`

`Created myfile.json`